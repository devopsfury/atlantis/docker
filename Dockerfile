# syntax=docker/dockerfile:1

ARG ATLANTIS_RELEASE=v0.25.0
ARG ARCH=linux_amd64
ARG ATLANTIS_URL=https://github.com/runatlantis/atlantis/releases/download/${ATLANTIS_RELEASE}/atlantis_${ARCH}.zip
ARG USERNAME=atlantis
ARG USER_UID=1000
ARG USER_GID=$USER_UID

FROM alpine:3.18 AS build

ARG USERNAME
ARG USER_UID
ARG USER_GID
ARG ATLANTIS_URL

RUN apk add --no-cache zip unzip wget \
    && wget -O /tmp/atlantis.zip $ATLANTIS_URL \
    && mkdir -p /app

WORKDIR /app
RUN unzip /tmp/atlantis.zip && rm -f /tmp/atlantis.zip


FROM alpine:3.18 AS main

ARG USERNAME
ARG USER_UID
ARG USER_GID

RUN addgroup -S --gid $USER_GID $USERNAME \
    && adduser -S -D -h /app -u $USER_UID -G $USERNAME $USERNAME

WORKDIR /app

COPY --from=build /app .

RUN chown -R $USERNAME:$USERNAME /app

USER $USERNAME
